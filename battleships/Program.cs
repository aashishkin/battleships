﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrissCross
{
    class Program
    {
        enum Cell { Fired, Ship, Empty, FiredShip }
        enum Step { Player1, Player2 }

        static void WritingMas(ref Cell[,] mas, int n)
        {
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    switch (mas[i, j])
                    {
                        case Cell.Fired:
                            Console.Write("{0, 2}", "х");
                            break;
                        case Cell.Ship:
                            if (n == 1) Console.Write("{0, 2}", "K"); else Console.Write("{0, 2}", "o");
                            break;
                        case Cell.Empty:
                            Console.Write("{0, 2}", "o");
                            break;
                        case Cell.FiredShip:
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.Write("{0, 2}", "X");
                            Console.ResetColor();
                            break;
                    }
                }
                Console.WriteLine();
            }
        }


        static void Main(string[] args)
        {
            #region variables
            int fieldSize = 10;
            Cell[,] field = new Cell[fieldSize, fieldSize];
            Cell[,] field2 = new Cell[fieldSize, fieldSize];
            Step currentStep = Step.Player1;
            bool playGame = true;
            int countFiredShipPlayer = 0;
            int countFiredShipBot = 0;
            Random rnd = new Random();
            #endregion

            #region clean arr
            for (int i = 0; i < fieldSize; i++)
            {
                for (int j = 0; j < fieldSize; j++)
                {
                    field[i, j] = Cell.Empty;
                    field2[i, j] = Cell.Empty;
                }
            }
            #endregion

            #region replase ships
            #region players ships
            int inputI = 0, inputJ = 0;
            for (int i = 0; i < 10; i++)
            {
                bool inputResult, inputResult2;
                do
                {
                    Console.Clear();
                    WritingMas(ref field, 1);
                    Console.WriteLine($"Разместите корабль №{i + 1}");
                    Console.Write("Введите номер строки: ");

                    inputResult = int.TryParse(Console.ReadLine(), out inputI);

                    inputI--;

                    Console.Write("Введите номер столбца: ");

                    inputResult2 = int.TryParse(Console.ReadLine(), out inputJ);

                    inputJ--;
                } while (inputResult == false || inputResult2 == false || inputI < 0 || inputJ < 0 || inputI > fieldSize - 1 || inputJ > fieldSize - 1 || field[inputI, inputJ] != Cell.Empty);
                field[inputI, inputJ] = Cell.Ship;
            }
            #endregion
            #region bots ships
            for (int i = 0; i < 10; i++)
            {
                int dx, dy;
                do
                {
                    dx = rnd.Next(0, 10);
                    dy = rnd.Next(0, 10);
                } while (field2[dx, dy] != Cell.Empty);
                field2[dx, dy] = Cell.Ship;
            }
            #endregion
            #endregion

            #region game loop
            while (playGame)
            {
                Console.Clear();
                #region field
                WritingMas(ref field, 1);
                Console.WriteLine();
                Console.WriteLine();
                WritingMas(ref field2, 0);

                #endregion

                #region whos win

                if (countFiredShipBot == 10)
                {
                    Console.WriteLine("Игрок победил!");
                    playGame = false;
                    continue;
                }
                else if (countFiredShipPlayer == 10)
                {
                    Console.WriteLine("Игрок проиграл!");
                    playGame = false;
                    continue;
                }
                #endregion

                #region players steps
                #region players shooting
                bool inputResult, inputResult2;
                if (currentStep == Step.Player1)
                {
                    do
                    {
                        Console.Write("Введите номер строки: ");

                        inputResult = int.TryParse(Console.ReadLine(), out inputI);

                        inputI--;

                        Console.Write("Введите номер столбца: ");

                        inputResult2 = int.TryParse(Console.ReadLine(), out inputJ);

                        inputJ--;
                    } while (inputResult == false || inputResult2 == false || inputI < 0 || inputJ < 0 || inputI > fieldSize - 1 || inputJ > fieldSize - 1 || (field2[inputI, inputJ] == Cell.Empty && field2[inputI, inputJ] == Cell.Ship));

                    if (field2[inputI, inputJ] == Cell.Ship)
                    {
                        field2[inputI, inputJ] = Cell.FiredShip;
                        countFiredShipBot++;
                    }
                    else
                    {
                        field2[inputI, inputJ] = Cell.Fired;
                    }
                    currentStep = Step.Player2;
                }
                #endregion
                #region bots shooting
                if (currentStep == Step.Player2)
                {
                    int botsShootX, botsShootY;
                    do
                    {
                        botsShootX = rnd.Next(0, 10);
                        botsShootY = rnd.Next(0, 10);
                    } while (field[botsShootX, botsShootY] == Cell.Fired || field[botsShootX, botsShootY] == Cell.FiredShip);
                    if (field[botsShootX, botsShootY] == Cell.Ship)
                    {
                        field[botsShootX, botsShootY] = Cell.FiredShip;
                        countFiredShipPlayer++;
                    }
                    else
                    {
                        field[botsShootX, botsShootY] = Cell.Fired;
                    }
                    currentStep = Step.Player1;
                }
                #endregion
                Console.WriteLine();
                Console.WriteLine($"У игрока осталось кораблей: {10 - countFiredShipPlayer}");
                Console.WriteLine();
                Console.WriteLine($"У бота осталось кораблей: {10 - countFiredShipBot}");
                Console.WriteLine("Введите <Enter> для начала следующего раунда");
                Console.ReadLine();
                #endregion
            }
            #endregion
            Console.ReadKey();
        }
    }
}
